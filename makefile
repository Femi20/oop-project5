string: driver.o mystring.o
	g++ -o string driver.o mystring.o
	
driver.o: mystring.h driver.cpp
	g++ -c driver.cpp
	
mystring.o: mystring.h mystring.cpp
	g++ -c mystring.cpp
	
clean:
	rm *.o string