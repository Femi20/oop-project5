#include "mystring.h"
#include <cstring>
#include <iomanip>



//STANDALONE FUNTIONS
ostream& operator<< (ostream& os , const MyString& s )
{
	
	os << s.string;
	
	return os;
}
istream& operator>> (istream& is, MyString& s)
{
	char buffer[10000];
	is >> buffer;
	
	s.size = strlen(buffer);
	delete [] s.string;
	
	s.string = new char[s.size+1];
	s.string[s.size] = '\0';
	
	for(int i =0; i < s.size; i++)
	{
		s.string[i] = buffer[i];
	}	
	
	return is;
	
	
}
istream& getline (istream& is, MyString& s, char delim)
{
	char buffer[10000];
	is.getline(buffer, 10000, delim);
	
	s.size = strlen(buffer);
	delete [] s.string;
	
	s.string = new char[s.size+1];
	s.string[s.size] = '\0';
	
	for(int i =0; i < s.size; i++)
	{
		s.string[i] = buffer[i];
	}	
	
	return is;
	
}

MyString operator+ (const MyString& s1, const MyString& s2 )
{
	int size = s1.getLength() + s2.getLength();
	
	char buffer[size+1];
	
	buffer[size] = '\0';
	
	strcpy(buffer, s1.string);
	strcat(buffer, s2.string);

	MyString s(buffer);
	
	return s;
}
	

 bool operator< (const MyString& s1 , const MyString& s2 )
 {
	int cmp = strcmp(s1.string, s2.string);
	
	return (cmp < 0);
	
 }
 bool operator> (const MyString& s1, const MyString& s2)
 {
	 return (s2 < s1);
 }
 bool operator<=(const MyString& s1, const MyString& s2)
 {
	 return !(s1 > s2);
 }
 bool operator>=(const MyString& s1, const MyString& s2)
 {
	  return !(s1 < s2);
 }
 bool operator==(const MyString& s1, const MyString& s2)
 {
	 return (strcmp(s1.string,s2.string) == 0);
 }
 bool operator!=(const MyString& s1, const MyString& s2)
 {
	 return !(s1 == s2);
 }

  
//MEMBER FUNCTIONS
MyString::MyString()// empty string
{
	  string = new char[1];
	  size =0;
	  string[0] = '\0';
}
  
MyString::MyString(const char* s)	// conversion from c-string
{
	string = new char[strlen(s)+1];
	size = strlen(s);
	string[size] = '\0';
	strcpy(string, s);
	
	
	
}


MyString::MyString(int num)			// conversion from int
{
	char c;
	if (num < 10)
	{
		size = 1;
		string = new char[size+1];
		string[size] = '\0';
		
		char c  = num + '0';
		
		string[0] = c;
	}
	int count = 0;
	if (num >= 10)
	{
		int temp = num;
		
		while(temp != 0)
		{
			count ++;
			temp = temp/10;
		}
		
		count++;
		size = count;
		
		string = new char [size+1];
		string[size] = '\0';
	}
	
	int temp = num;

		for(int i = size - 2; i >= 0; i--)
		{
			c = (temp%10) + '0';
			string[i] = c; 
			temp = temp/10;
		}
	
	}
		


MyString::~MyString() // destructor
{
	 delete [] string;
	 size =0;
	 
}

MyString::MyString(const MyString& s)		// copy constructor
{
	  size = s.size;
		
	delete [] string;
	
	string = new char [size+1];
	string[size] = '\0';
	for(int i = 0;  i < size; i++)
	{
		string[i] = s[i];
	}
	
	  
}

MyString& MyString:: operator=(const MyString& s )  // assignment operator
{
	 if (this != &s)
	 {
		delete [] string;
		 
		size = s.size;
		
		string = new char[size+1];
		
		
		for(int i = 0;  i < size; i++)
		{
			string[i] = s.string[i];
		}
		
		string[size] = '\0';
	 }

	return *this;
}

MyString& MyString:: operator+=(const MyString& s) // concatenation/assignment
{
	
	*this = *this + s;
	
	return *this;
}

// bracket operators to access char positions
char& MyString:: operator[] (unsigned int index)
{
	if(index < size)
	{
		return string[index];
	}
	
	char * buffer = new char [index+2];
	buffer[index] = '\0';
	
	//Copies current string into buffer
	for(int i = 0; i < size; i++)
	{
		buffer[i] = string[i];
	}
	int i =  size;
	//puts spaces between string and null character
	while(buffer[i] != '\0')
	{
		buffer[i] = ' ';
		i++;
	}
	
	delete [] string;
	
	string  = new char [index+2];
	size = index+1;
	for(int i = 0; i < index+2; i++)
	{
		string[i] = buffer[i];
	}
	
	delete [] buffer;
	
	return string[index];
	
}
const char& MyString:: operator[] (unsigned int index) const
{
	if(index < size)
	 {
		 return string[index];
	 }
	 return string[size];
	  
}

// insert s into the string at position "index"
MyString& MyString::insert(unsigned int index, const MyString& s)
{

	if(index < size && index >= 0)
	{
		char tempBuffer[size-index];
		
		//Storing part of string being replaced
		for(int i = index; i < size; i++)
		{
			tempBuffer[i-index] = string[i];
		}
		
		Grow(s.getLength());
		
		

		//Inserting new string
		int count = 0;
		for(int j = index; j < index + s.size; j++)
		{
			string[j] = s[j-s.size];
		}
		
		strcat(string, tempBuffer);
		
		return *this;
	}
	
	//Attaching replaced string
	else if(index >= getLength() && index >= 0)
	{
	
		Grow(s.size);
			
		strcat(string, s.string);
		
		return *this;
	}	
		
	
	  
}

// find index of the first occurrence of s inside the string
//  return the index, or -1 if not found
int MyString::indexOf(const MyString& s) const
{	
	MyString comp;
	int occ = 0;
	for (int i = 0; i <= size-s.size; i++)
	{
		comp = substring(i,s.getLength());
		if(comp == s)
		{
			occ = i;
			return occ;
		}
	}
 	  return -1;	  
}
  
int MyString::getLength() const	// return string size
{
	 return size; 
	  
}

const char*  MyString::getCString() const	// return c-string equiv
{ 

	char * buffer = new char[size];
	
	strcpy(buffer, string);
	buffer[size-1] = '\0';

	delete [] buffer;
  
	return buffer; 
	  
}

MyString  MyString::substring(unsigned int x, unsigned int y ) const
{
	  MyString res;
	  res.Grow(y);
	 
	  
	  for(int i = 0; i < res.size; i++)
	  {
		  res.string[i] = string[i+x];
	  }

	  return res; 
}

MyString  MyString::substring(unsigned int x) const
{
	  
	  MyString res;
	  res.Grow(size-x+1);
	  
	  int i = x;
	  int count = 0;
	  while(string[i] != '\0')
	  {
		res.string[count] = string[i];
		i++;
		count++;
	  }
	  res.string[count] = '\0';

	  return res; 
	 
}

void MyString::Grow (int s)	//
{
	int tempSize = size;
	size += s;
	
	char * buffer = new char[size+1];
	buffer[size] = '\0';

	for(int i =0; i < tempSize; i++)
	{
		buffer[i] = string[i];
	}
	
	delete [] string;
	
	string = buffer;
	
}
